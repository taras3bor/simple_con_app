﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Text.Json;

namespace simple_con_app
{
    class Program
    {
        private static readonly NLog.Logger LOG = NLog.LogManager.GetCurrentClassLogger();

        [Obsolete]
        static void Main(string[] args)
        {
            NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(@"res\nlog.config", true);

            string jsnstr = "{\"fontName\":\"Arial\",\"fontSize\":22,\"nullValueKey\":null,\"textNumber\":\"2147000123\"}";

            using (JsonDocument jDocument = JsonDocument.Parse(jsnstr))
            {
                var elem = jDocument.RootElement.GetProperty("fontName");
                //LOG.Info($"fontName:{elem.GetString()}");

                int numb;
                JsonGetIntValue(out numb, jDocument.RootElement,"notExistKey");
                JsonGetIntValue(out numb, jDocument.RootElement,"nullValueKey");
                JsonGetIntValue(out numb, jDocument.RootElement, "fontName");
                JsonGetIntValue(out numb, jDocument.RootElement, "fontSize");
                JsonGetIntValue(out numb, jDocument.RootElement, "textNumber");
                string sstr;
                JsonGetStringValue(out sstr, jDocument.RootElement, "notExistKey");
                JsonGetStringValue(out sstr, jDocument.RootElement, "nullValueKey");
                JsonGetStringValue(out sstr, jDocument.RootElement, "fontName");
                JsonGetStringValue(out sstr, jDocument.RootElement, "fontSize");
                JsonGetStringValue(out sstr, jDocument.RootElement, "textNumber");



            }

            while (true)
            {
                LOG.Info("Press [q] to quit...");
                var cki = Console.ReadKey();
                if (cki.KeyChar == 'q')
                    break;
                LOG.Info($"Pressed {cki.KeyChar}");
                Thread.Sleep(200);
                //Thread.Sleep(500);
            }

        }


        public static bool JsonGetIntValue(out int value, JsonElement elem, string key, int defaultValue=0)
        {
            try
            {
                var jKey = elem.GetProperty(key);
                var kind = jKey.ValueKind;
                if(kind == JsonValueKind.String)
                {
                    value = int.Parse(jKey.GetString());
                    return true;
                }else if(kind == JsonValueKind.Number)
                {
                    value = jKey.GetInt32();
                    return true;
                }
                else
                    throw new Exception("Value not number");
            }
            catch (Exception ex)
            {
                LOG.Warn($"JGIV exc: {ex.Message}. Key ['{key}'] return default({defaultValue})");
                value = defaultValue;
                return false;
            }
        }

        public static bool JsonGetStringValue(out string value, JsonElement elem, string key, string defaultValue = "")
        {
            try
            {
                var jKey = elem.GetProperty(key);
                var kind = jKey.ValueKind;
                if (kind == JsonValueKind.Number)
                {
                    value = jKey.GetInt64().ToString();
                    return true;
                } else if (kind == JsonValueKind.String)
                {
                    value = jKey.GetString();
                    return true;
                }
                else
                    throw new Exception("Value not string");
            }
            catch (Exception ex)
            {
                LOG.Warn($"JGSV exc: {ex.Message}. Key ['{key}'] return default({defaultValue})");
                value = defaultValue;
                return false;
            }
        }
    }
}
